import { useCallback, useEffect, useRef, useState } from "react";

const api = process.env.REACT_APP_API;

const useFetch: <T>(
  url: string
) => [T | undefined, boolean, Error | undefined] = <T>(url: string) => {
  const [data, setData] = useState<T>();
  const [error, setError] = useState<Error>();
  const [loading, setLoading] = useState(true);
  const isMountedRef = useRef(false);

  const fetchData = useCallback(async () => {
    try {
      const response = await fetch(`${api ?? ""}${url}`);

      if (!response.ok) throw new Error();

      const newData = await response.json();
      if (isMountedRef.current) setData(newData);
    } catch (e) {
      if (isMountedRef.current) setError(e);
    } finally {
      if (isMountedRef.current) setLoading(false);
    }
  }, [url]);

  useEffect(() => {
    isMountedRef.current = true;
    fetchData();

    return () => {
      isMountedRef.current = false;
    };
  }, [fetchData]);

  return [data, loading, error];
};

export default useFetch;
