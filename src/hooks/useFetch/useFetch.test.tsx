import { render, waitFor, screen } from "@testing-library/react";
import { rest } from "msw";
import { setupServer } from "msw/node";
import useFetch from "./useFetch.hook";

const server = setupServer(
  rest.get("/test", (req, res, ctx) => {
    return res(ctx.json({ hello: "world" }));
  }),

  rest.get("/error", (req, res, ctx) => {
    return res.networkError("");
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

const UseFetch = ({ url }: { url: string }) => {
  const [data, loadingData, error] = useFetch(url);

  return (
    <>
      {loadingData && <div>loading</div>}
      {!!data && <div>data</div>}
      {error && <div>error</div>}
    </>
  );
};

describe("useFetch", () => {
  test("it shows loading", () => {
    const { getByText } = render(<UseFetch url="/test" />);

    expect(getByText("loading")).toBeInTheDocument();
  });

  test("it shows data", async () => {
    render(<UseFetch url="/test" />);

    await waitFor(() => screen.getByText("data"));

    expect(screen.getByText("data")).toBeInTheDocument();
  });

  test("it shows error", async () => {
    render(<UseFetch url="/error" />);

    await waitFor(() => screen.getByText("error"));

    expect(screen.getByText("error")).toBeInTheDocument();
  });
});
