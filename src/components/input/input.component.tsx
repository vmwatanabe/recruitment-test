import { ReactComponent as SearchIcon } from "../../icons/search.svg";
import { InputContainer } from "./input.styles";

const Input: React.FC = () => {
  return (
    <InputContainer data-testid="input-container">
      <SearchIcon />
      <input type="text" placeholder="Buscar" />
    </InputContainer>
  );
};

export default Input;
