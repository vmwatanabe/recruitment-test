import styled from "styled-components";

export const InputContainer = styled.div`
  background: white;
  border-radius: 5px;
  padding: 9px 9px 9px 31px;
  border: 1px solid #cccfde;
  position: relative;

  svg {
    width: 14px;
    height: 14px;
    position: absolute;
    top: 9px;
    left: 11px;
    fill: #117eff;
  }

  input {
    height: 13px;

    border: none;
    background-image: none;
    background-color: transparent;
    -webkit-box-shadow: none;
    -moz-box-shadow: none;
    box-shadow: none;
    outline: none;
  }
`;
