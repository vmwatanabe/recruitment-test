import { render } from "@testing-library/react";
import Input from "./input.component";

describe("input", () => {
  test("it renders its container", () => {
    const { getByTestId } = render(<Input />);

    expect(getByTestId("input-container")).toBeInTheDocument();
  });
});
