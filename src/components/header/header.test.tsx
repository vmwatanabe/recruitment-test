import { render } from "@testing-library/react";
import Header from "./header.component";

describe("header", () => {
  test("it renders its container", () => {
    const { getByTestId } = render(<Header />);

    expect(getByTestId("header-container")).toBeInTheDocument();
  });
});
