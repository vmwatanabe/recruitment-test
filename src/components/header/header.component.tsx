import Input from "../input/input.component";

import AcmeLogo from "../../assets/acme-logo.png";
import {
  HeaderContainer,
  Container,
  UserInitial,
  ImgContainer,
  SButton,
} from "./header.styles";

const defaultInitial = "W";

const Header: React.FC = () => {
  return (
    <HeaderContainer data-testid="header-container">
      <Container>
        <UserInitial>
          <span>{defaultInitial}</span>
        </UserInitial>
        <ImgContainer>
          <img src={AcmeLogo} alt="acme" />
        </ImgContainer>
      </Container>
      <Container>
        <Input />
        <SButton icon="add" text="Nova Jornada" color="#117EFF" />
      </Container>
    </HeaderContainer>
  );
};

export default Header;
