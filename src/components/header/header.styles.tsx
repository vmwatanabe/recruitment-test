import styled from "styled-components";
import Button from "../button/button.component";

export const SButton = styled(Button)``;

export const Container = styled.div`
  display: flex;
  align-items: center;

  ${SButton} {
    margin-left: 15px;
  }
`;

export const HeaderContainer = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 61px;
`;

export const UserInitial = styled.div`
  background: #117eff;
  width: 24px;
  height: 24px;
  border-radius: 50%;
  color: white;
  font-size: 14px;
  font-weight: bold;
  display: flex;

  span {
    margin: auto;
  }
`;

export const ImgContainer = styled.div`
  padding: 2px 11px;
  margin-left: 21px;
  border: 1px solid #ebeef6;
  border-radius: 5px;
  background: white;

  img {
    width: 66px;
  }
`;
