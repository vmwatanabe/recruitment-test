import { useMemo } from "react";
import Journey from "./components/journey/journey.component";
import { Journey as JourneyType } from "../../types";
import {
  JourneysListContainer,
  JourneysHeader,
  JourneysHeaderCell,
} from "./journeys-list.styles";

interface JourneysListProps {
  items?: JourneyType[];
}

const JourneysList: React.FC<JourneysListProps> = ({ items }) => {
  const journeys = useMemo(() => {
    if (!items?.length) return null;

    return items.map((item) => <Journey {...item} key={item.id} />);
  }, [items]);

  return (
    <JourneysListContainer data-testid="journeys-list-container">
      <JourneysHeader>
        <JourneysHeaderCell columns="3">
          <span>Nome</span>
        </JourneysHeaderCell>
        <JourneysHeaderCell>
          <span>Destinatários</span>
        </JourneysHeaderCell>
        <JourneysHeaderCell>
          <span>Sucesso</span>
        </JourneysHeaderCell>
        <JourneysHeaderCell>
          <span>Status</span>
        </JourneysHeaderCell>
      </JourneysHeader>
      {journeys}
    </JourneysListContainer>
  );
};

export default JourneysList;
