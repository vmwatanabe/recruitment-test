import styled from "styled-components";

export const StatusContainer = styled.span`
  display: flex;
  align-items: center;

  svg {
    width: 14px;
    height: 14px;
    margin-right: 8px;
  }
`;

export const JourneyContainer = styled.div`
  width: 100%;
  padding: 16px;
  background: white;
  font-size: 13px;

  &:not(:first-child) {
    margin-top: 10px;
  }
`;

export const JourneyRow = styled.div`
  display: flex;
`;

interface JourneyCellProps {
  columns?: string;
  bold?: boolean;
}

export const JourneyCell = styled.div<JourneyCellProps>`
  flex: ${({ columns }) => columns ?? "1"};
  display: flex;

  font-weight: ${({ bold }) => (bold ? "bold" : "normal")};
  span {
    text-align: center;
  }
`;
