import { useContext } from "react";
import MainContext from "../../../../contexts/main-context/main.context";
import { getStatus } from "./journey.helpers";
import { JourneyContainer, JourneyRow, JourneyCell } from "./journey.styles";

interface JourneyProps {
  name?: string;
  recipients?: string;
  status?: number;
  success?: string;
  id?: string;
}

const DEFAULT_TEXT = "-";

const Journey: React.FC<JourneyProps> = ({
  name,
  recipients,
  status,
  success,
  id,
}) => {
  const { filters } = useContext(MainContext);

  const renderPropText = (prop?: string | number) => {
    if (prop === undefined) return DEFAULT_TEXT;

    return prop;
  };

  const renderStatus = () => {
    if (!(status && filters)) return DEFAULT_TEXT;

    const statusContent = getStatus(status, filters);

    if (!statusContent) return DEFAULT_TEXT;

    return statusContent;
  };

  return (
    <JourneyContainer data-testid="journey-container">
      <JourneyRow>
        <JourneyCell data-testid="journey-name" columns="3" bold>
          <span>{renderPropText(name)}</span>
        </JourneyCell>
        <JourneyCell data-testid="journey-recipients">
          <span>{renderPropText(recipients)}</span>
        </JourneyCell>
        <JourneyCell data-testid="journey-success">
          <span>{renderPropText(success)}</span>
        </JourneyCell>
        <JourneyCell data-testid="journey-status">
          <span>{renderStatus()}</span>
        </JourneyCell>
      </JourneyRow>
    </JourneyContainer>
  );
};

export default Journey;
