import { render } from "@testing-library/react";
import Journey from "./journey.component";

describe("journey", () => {
  test("it renders its container", () => {
    const { getByTestId } = render(<Journey />);
    expect(getByTestId("journey-container")).toBeInTheDocument();
  });

  test("it renders name", () => {
    const { getByTestId } = render(<Journey />);
    expect(getByTestId("journey-name")).toBeInTheDocument();
  });

  test("it renders recipients", () => {
    const { getByTestId } = render(<Journey />);
    expect(getByTestId("journey-recipients")).toBeInTheDocument();
  });

  test("it renders success rate", () => {
    const { getByTestId } = render(<Journey />);
    expect(getByTestId("journey-success")).toBeInTheDocument();
  });

  test("it renders status", () => {
    const { getByTestId } = render(<Journey />);
    expect(getByTestId("journey-status")).toBeInTheDocument();
  });
});
