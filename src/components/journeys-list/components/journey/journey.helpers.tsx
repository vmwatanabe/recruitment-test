import { getIcon } from "../../../../helpers/icons";
import { Filter } from "../../../../types";
import { StatusContainer } from "./journey.styles";

export const getStatus = (status: number, filters: Filter[]) => {
  const icon = getIcon(status);

  const currentFilter = filters.find((filter) => filter.id === status);

  return (
    <StatusContainer>
      {icon} <span>{currentFilter?.name}</span>
    </StatusContainer>
  );
};
