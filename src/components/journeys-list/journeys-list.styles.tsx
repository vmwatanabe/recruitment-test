import styled from "styled-components";

export const JourneysListContainer = styled.div`
  width: 699px;
  position: relative;
`;

export const JourneysHeader = styled.div`
  display: flex;
  color: #9196ab;
  font-size: 13px;
  padding: 0 16px;
  margin-bottom: 11px;
`;

interface JourneysHeaderCellProps {
  columns?: string;
}

export const JourneysHeaderCell = styled.div<JourneysHeaderCellProps>`
  flex: ${({ columns }) => columns ?? "1"};
  display: flex;

  span {
    text-align: center;
  }
`;
