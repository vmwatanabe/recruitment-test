import { render } from "@testing-library/react";
import JourneysList from "./journeys-list.component";

describe("journeys-list", () => {
  test("it renders its container", () => {
    const { getByTestId } = render(<JourneysList />);
    expect(getByTestId("journeys-list-container")).toBeInTheDocument();
  });
});
