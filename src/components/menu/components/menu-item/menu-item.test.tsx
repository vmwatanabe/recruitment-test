import { fireEvent, render, screen } from "@testing-library/react";
import MenuItem from "./menu-item.component";

const defaultProps = {
  id: 1,
  name: "name",
  quantity: 1,
};

describe("menu-item", () => {
  test("it renders its container", () => {
    const { getByTestId } = render(<MenuItem {...defaultProps} />);

    expect(getByTestId("menu-item-container")).toBeInTheDocument();
  });

  test("it renders title", () => {
    render(<MenuItem {...defaultProps} />);
    const title = screen.getByText(defaultProps.name);

    expect(title).toBeInTheDocument();
  });

  test("it renders quantity", () => {
    const { getByTestId } = render(<MenuItem {...defaultProps} />);

    expect(getByTestId("menu-item-quantity")).toBeInTheDocument();
  });

  test("it renders icon", () => {
    const { getByTestId } = render(<MenuItem {...defaultProps} />);

    expect(getByTestId("menu-item-icon")).toBeInTheDocument();
  });

  test("it renders default title when no title is provided", () => {
    render(<MenuItem quantity={0} />);
    const title = screen.getByText(/unknown/i);

    expect(title).toBeInTheDocument();
  });

  test("it renders default quantity if no value is provided", () => {
    const { getByTestId } = render(<MenuItem quantity={0} />);
    const quantity = getByTestId("menu-item-quantity");

    expect(quantity).toHaveTextContent("0");
  });

  test("it calls onClick when clicked", () => {
    const handleClick = jest.fn();
    render(<MenuItem onClick={handleClick} />);
    fireEvent.click(screen.getByTestId("menu-item-container"));
    expect(handleClick).toHaveBeenCalledTimes(1);
  });

  test("it has the active class when active", () => {
    const { getByTestId } = render(<MenuItem {...defaultProps} isActive />);

    expect(getByTestId("menu-item-container")).toHaveClass("isActive");
  });
});
