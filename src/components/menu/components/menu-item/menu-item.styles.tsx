import styled from "styled-components";

export const MenuItemTitle = styled.span`
  margin-right: 13px;
`;
export const MenuItemQuantity = styled.span`
  color: #9196ab;
  font-size: 10px;
  width: 22px;
  height: 22px;
  border-radius: 50%;
  display: flex;
  align-items: center;
  background: #e4e6f1;
  position: relative;
  margin-left: auto;

  span {
    margin: auto;
  }
`;
export const MenuItemIcon = styled.span`
  margin-right: 10px;
  width: 16px;
  height: 16px;
`;

export const MenuItemContainer = styled.div`
  font-size: 13px;
  display: flex;
  align-items: center;
  cursor: pointer;

  &.isActive {
    font-weight: bold;

    ${MenuItemTitle} {
      color: #117eff;
    }

    ${MenuItemQuantity} {
      background: #117eff;
      color: white;
    }
  }
`;
