import classNames from "classnames";
import { getIcon } from "../../../../helpers/icons";
import {
  MenuItemContainer,
  MenuItemIcon,
  MenuItemTitle,
  MenuItemQuantity,
} from "./menu-item.styles";

interface MenuItemProps {
  id?: number;
  className?: string;
  name?: string;
  quantity?: number;
  onClick?: (id?: number) => any;
  isActive?: boolean;
}

const MenuItem: React.FC<MenuItemProps> = ({
  id,
  className,
  name = "(unknown)",
  quantity = 0,
  onClick,
  isActive,
}) => {
  const handleClick = () => {
    if (!onClick) return;

    onClick(id);
  };

  return (
    <MenuItemContainer
      className={classNames(className, { isActive })}
      data-testid="menu-item-container"
      onClick={handleClick}
    >
      <MenuItemIcon data-testid="menu-item-icon">{getIcon(id)}</MenuItemIcon>
      <MenuItemTitle data-testid="menu-item-title">{name}</MenuItemTitle>
      <MenuItemQuantity data-testid="menu-item-quantity">
        <span>{quantity}</span>
      </MenuItemQuantity>
    </MenuItemContainer>
  );
};

export default MenuItem;
