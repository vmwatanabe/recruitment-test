import { render, screen, fireEvent } from "@testing-library/react";
import Menu from "./menu.component";

const defaultProps = {
  items: [
    {
      id: 1,
      name: "name",
      quantity: 1,
    },
  ],
  onItemClick: () => null,
};

describe("menu", () => {
  test("it renders its container", () => {
    const { getByTestId } = render(<Menu {...defaultProps} />);

    expect(getByTestId("menu-container")).toBeInTheDocument();
  });

  test("it renders all its menu-items", () => {
    const { getAllByTestId } = render(<Menu {...defaultProps} />);

    expect(getAllByTestId("menu-item-container")).toHaveLength(
      defaultProps.items.length
    );
  });

  test("it calls menu item onClick event", () => {
    const handleClick = jest.fn();
    render(<Menu {...defaultProps} onItemClick={handleClick} />);

    fireEvent.click(screen.getByTestId("menu-item-container"));

    expect(handleClick).toHaveBeenCalledTimes(1);
  });
});
