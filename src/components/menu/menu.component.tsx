import { useMemo } from "react";
import { Filter } from "../../types";
import { MenuContainer, SMenuItem } from "./menu.styles";

interface MenuProps {
  items?: Filter[];
  current?: number;
  onItemClick: (id?: number) => any;
}

const Menu: React.FC<MenuProps> = ({ items, onItemClick, current }) => {
  const menuItems = useMemo(() => {
    if (!items?.length) return null;

    return items.map((item) => (
      <SMenuItem
        {...item}
        isActive={current === item.id}
        key={item.id}
        onClick={onItemClick}
      />
    ));
  }, [items, onItemClick, current]);

  return (
    <MenuContainer data-testid="menu-container">{menuItems}</MenuContainer>
  );
};

export default Menu;
