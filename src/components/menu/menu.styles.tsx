import styled from "styled-components";
import MenuItem from "./components/menu-item/menu-item.component";

export const SMenuItem = styled(MenuItem)``;

export const MenuContainer = styled.div`
  ${SMenuItem} {
    &:not(:first-child) {
      margin-top: 11px;
    }
  }
`;
