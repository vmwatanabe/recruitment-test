import styled from "styled-components";

export const Text = styled.div`
  color: white;
  font-size: 13px;
  font-weight: bold;
`;

export const Icon = styled.div`
  margin-right: 5px;
  display: flex;

  svg {
    width: 14px;
    height: 14px;
    fill: white;
  }
`;

interface ButtonContainerProps {
  color: string;
}

export const ButtonContainer = styled.div<ButtonContainerProps>`
  background: ${({ color }) => color};
  border-radius: 5px;
  padding: 10px 20px;
  display: flex;
  align-items: center;
`;
