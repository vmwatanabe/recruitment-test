import { render } from "@testing-library/react";
import Button from "./button.component";

const defaultProps = {
  text: "title",
  color: "red",
};

describe("button", () => {
  test("it renders its container", () => {
    const { getByTestId } = render(<Button {...defaultProps} />);

    expect(getByTestId("button-container")).toBeInTheDocument();
  });
});
