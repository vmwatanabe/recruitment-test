import { ReactComponent as PlusIcon } from "../../icons/plus.svg";
import { ButtonContainer, Text, Icon } from "./button.styles";

interface ButtonProps {
  className?: string;
  text: string;
  color: string;
  icon?: "add";
}

const Button: React.FC<ButtonProps> = ({ className, text, color, icon }) => {
  const getIcon = () => {
    if (icon === "add")
      return (
        <Icon>
          <PlusIcon />
        </Icon>
      );
  };

  return (
    <ButtonContainer
      className={className}
      data-testid="button-container"
      color={color}
    >
      {getIcon()}
      <Text>{text}</Text>
    </ButtonContainer>
  );
};

export default Button;
