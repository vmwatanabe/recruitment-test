import Home from "./pages/home/home.component";
import "./reset.css";
import "./App.css";

function App() {
  return <Home />;
}

export default App;
