import { ReactComponent as TableIcon } from "../icons/table.svg";
import { ReactComponent as PlaneIcon } from "../icons/paper-plane.svg";
import { ReactComponent as PlayCircleIcon } from "../icons/play-circle.svg";
import { ReactComponent as PenIcon } from "../icons/pen.svg";
import { ReactComponent as BedIcon } from "../icons/bed.svg";
import { ReactComponent as CheckIcon } from "../icons/check.svg";

const icons: { [key: number]: any } = {
  0: <TableIcon fill="#D190DD" />,
  1: <PlaneIcon fill="#C1CA4F" />,
  2: <PlayCircleIcon fill="#35C667" />,
  3: <PenIcon fill="#3FA8F4" />,
  4: <BedIcon fill="#EBBD3E" />,
  5: <CheckIcon fill="#9FABD5" />,
};

export const getIcon = (id?: number) => {
  if (id === undefined) return null;

  return icons[id] ?? null;
};
