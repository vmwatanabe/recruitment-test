import { render } from "@testing-library/react";
import Home from "./home.component";

describe("home", () => {
  test("it render its container", () => {
    const { getByTestId } = render(<Home />);

    expect(getByTestId("home-container")).toBeInTheDocument();
  });

  test("it renders header", () => {
    const { getByTestId } = render(<Home />);

    expect(getByTestId("header-container")).toBeInTheDocument();
  });

  test("it renders menu", () => {
    const { getByTestId } = render(<Home />);

    expect(getByTestId("menu-container")).toBeInTheDocument();
  });

  test("it renders journeys-list", () => {
    const { getByTestId } = render(<Home />);

    expect(getByTestId("journeys-list-container")).toBeInTheDocument();
  });
});
