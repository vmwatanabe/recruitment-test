import styled from "styled-components";

export const LeftContainer = styled.div``;
export const RightContainer = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

export const HomeContainer = styled.div`
  padding: 27px 90px;
  background: #fafbff;
`;

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Content = styled.div`
  display: flex;
`;

export const PageTitle = styled.h1`
  font-weight: bold;
  font-size: 16px;
  margin-bottom: 30px;
`;
