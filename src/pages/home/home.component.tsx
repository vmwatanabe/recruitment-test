import { useState } from "react";
import Header from "../../components/header/header.component";
import JourneysList from "../../components/journeys-list/journeys-list.component";
import Menu from "../../components/menu/menu.component";
import useFetch from "../../hooks/useFetch/useFetch.hook";
import MainContext from "../../contexts/main-context/main.context";
import { Filter, Journey } from "../../types";
import {
  HomeContainer,
  LeftContainer,
  RightContainer,
  MainContainer,
  Content,
  PageTitle,
} from "./home.styles";

const Home: React.FC = () => {
  const [filter, setFilter] = useState(0);

  const [filters] = useFetch<Filter[]>("filter");
  const [journeys] = useFetch<Journey[]>(`journey/${filter || ""}`);

  const handleMenuItemClick = (id?: number) => {
    if (id !== undefined) setFilter(id);
  };

  return (
    <MainContext.Provider value={{ filters }}>
      <HomeContainer data-testid="home-container">
        <Header />
        <MainContainer>
          <PageTitle>Jornadas</PageTitle>
          <Content>
            <LeftContainer>
              <Menu
                current={filter}
                items={filters}
                onItemClick={handleMenuItemClick}
              />
            </LeftContainer>
            <RightContainer>
              <JourneysList items={journeys} />
            </RightContainer>
          </Content>
        </MainContainer>
      </HomeContainer>
    </MainContext.Provider>
  );
};

export default Home;
