import { createContext } from "react";
import { Filter } from "../../types";

interface MainContextParams {
  filters?: Filter[];
}

const MainContext = createContext<MainContextParams>({});

export default MainContext;
