export type Filter = {
  id: number;
  name?: string;
  quantity?: number;
};

export type Journey = {
  name: string;
  status: number;
  recipients: string;
  success: string;
  id: string;
};
